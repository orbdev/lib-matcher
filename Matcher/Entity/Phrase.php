<?php

namespace Matcher\Entity;

class Phrase
{
    /**
     * @todo При внедрении заменить значения на значения из базы для каждого типа
     */
    const TYPE_SIMPLE = __LINE__;
    const TYPE_REGULAR = __LINE__;
    const TYPE_SMART = __LINE__;

    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $phrase;

    /**
     * @var array|null
     */
    protected $penalties;

    /**
     * @var mixed|null
     */
    protected $type;

    /**
     * @param int|null $id
     * @param string|null $phrase
     * @param mixed|null $type
     * @param array|null $penalties
     */
    public function __construct($id, $phrase, $type, $penalties = null)
    {
        $this->setId($id)
            ->setPhrase($phrase)
            ->setType($type)
            ->setPenalties($penalties);
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string|null $phrase
     * @return $this
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

    /**
     * @param array|null $penalties
     * @return $this
     */
    public function setPenalties($penalties)
    {
        $this->penalties = $penalties;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getPenalties()
    {
        return $this->penalties;
    }

    /**
     * @param mixed|null $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getType()
    {
        return $this->type;
    }
}