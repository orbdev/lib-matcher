<?php

namespace Matcher;

use Matcher\Exception\InvalidConfigurationException;
use Matcher\Entity\Phrase;
use Matcher\Exception\RuntimeException;

class Matcher
{
    /**
     * @var array|null
     */
    private $transformConfig;

    /**
     * @var Phrase[]
     */
    private $phrases;

    /**
     * Фразы, уже прошедшие через трансформеры
     * @var string[]
     */
    public $transformedPhrasesCache = [];

    /**
     * @param array|null $transformConfig
     * @param array|null $phrases
     */
    public function __construct($transformConfig = null, $phrases = null)
    {
        $this->setTransformConfig($transformConfig);
        $this->setPhrases($phrases);
    }

    /**
     * @param array $transformConfig
     */
    public function setTransformConfig($transformConfig)
    {
        $this->transformConfig = $transformConfig;
    }

    /**
     * @param Phrase[] $phrases
     */
    public function setPhrases($phrases)
    {
        $this->phrases = $phrases;
    }

    /**
     * @var TransformerInterface[]
     */
    private $transformers = [];

    /**
     * @param string $message
     * @throws InvalidConfigurationException
     * @throws RuntimeException
     * @return Phrase[]
     */
    public function match($message)
    {
        $this->checkConfiguration();

        $result = [];

        foreach ($this->phrases as $phrase) {

            if (!in_array($phrase->getType(), [Phrase::TYPE_SIMPLE, Phrase::TYPE_REGULAR, Phrase::TYPE_SMART])) {
                throw new RuntimeException(sprintf('Invalid phrase type "%s"', $phrase->getType()));
            }

            // Отдельная проверка для типа регулярного выражения. Если ее не произвести, система в случае невалидного
            // регулярного выражения проверит его как обычное совпадение и не найдет совпадений, при этом не будет ошибки.
            if ($phrase->getType() == Phrase::TYPE_REGULAR && !$this->isPhraseRegExp($phrase->getPhrase())) {
                throw new RuntimeException(sprintf(
                    'Phrase #%s "%s" has type of regular expression but does not contain valid regular expression in body',
                    $phrase->getId(),
                    $phrase->getPhrase()
                ));
            }

            $matchedPhrase = $this->matchWithTransformers($message, $phrase);

            if ($matchedPhrase) {
                $result[] = $matchedPhrase;
            }
        }

        return array_values($result);
    }

    /**
     * @param string $message
     * @param Phrase $phrase
     * @return Phrase|null
     * @throws RuntimeException
     */
    private function matchWithTransformers($message, Phrase $phrase)
    {
        $result = null;
        foreach ($this->transformConfig[$phrase->getType()] as $groupIndex => $transformGroup) {
            $transformedMessage = $message;
            $transformedPhrase = $this->getTransformedPhrase($phrase, $groupIndex);

            foreach ($transformGroup['message'] as $transformerClass) {
                $transformer = $this->getTransformer($transformerClass);
                $transformedMessage = $transformer->transform($transformedMessage);
            }

            if ($this->isPhraseRegExp($transformedPhrase)) {
                if ($this->matchRegular($transformedMessage, $transformedPhrase)) {
                    $result = $phrase;
                }
            } else {
                if ($this->matchSimple($transformedMessage, $transformedPhrase)) {
                    $result = $phrase;
                }
            }
        }

        return $result;
    }

    /**
     * Производит трансформации фразы, кеширует ее для группы и при следующих вызовах отдает нормализованную версию
     *
     * @param Phrase $phrase
     * @param $transformGroupIndex
     * @return mixed
     */
    private function getTransformedPhrase(Phrase $phrase, $transformGroupIndex)
    {
        if (empty($this->transformedPhrasesCache[$phrase->getId()][$transformGroupIndex])) {
            $transformedPhrase = $phrase->getPhrase();
            foreach ($this->transformConfig[$phrase->getType()][$transformGroupIndex]['phrase'] as $transformerClass) {
                $transformer = $this->getTransformer($transformerClass);
                $transformedPhrase = $transformer->transform($transformedPhrase);
            }

            $this->transformedPhrasesCache[$phrase->getId()][$transformGroupIndex] = $transformedPhrase;
        }

        return $this->transformedPhrasesCache[$phrase->getId()][$transformGroupIndex];
    }

    /**
     * @param string $phrase
     * @param string $message
     * @return bool
     */
    private function matchSimple($message, $phrase)
    {
        return mb_strpos($message, $phrase) !== false;
    }

    /**
     * @param string $phrase
     * @param string $message
     * @throws RuntimeException
     * @return bool
     */
    private function matchRegular($message, $phrase)
    {
        if (!$this->isPhraseRegExp($phrase)) {
            throw new RuntimeException(sprintf('Phrase "%s" is not a valid regular exception', $phrase));
        }

        if (preg_match($phrase, $message, $matches)) {
            return true;
        }
        return false;
    }

    /**
     * @throws InvalidConfigurationException
     */
    private function checkConfiguration()
    {
        if (is_null($this->transformConfig)) {
            throw new InvalidConfigurationException('Transformation config is not set');
        }

        if (is_null($this->phrases)) {
            throw new InvalidConfigurationException('Phrases are not set');
        }
    }

    /**
     * @param string $className
     * @return TransformerInterface
     */
    private function getTransformer($className)
    {
        if (!isset($this->transformers[$className])) {
            $this->transformers[$className] = new $className;
        }
        return $this->transformers[$className];
    }

    /**
     * Не самый элегантный код, но единственный верный способ определить, является ли строка регулярным выражением
     *
     * @param string $phrase
     * @return bool
     */
    private function isPhraseRegExp($phrase)
    {
        return @preg_match($phrase, null) === false ? false : true;
    }
}