<?php

namespace Matcher\Transformer;

use Matcher\TransformerInterface;

class LowerCaseTransformer implements TransformerInterface
{
    public function transform($string)
    {
        return mb_strtolower($string);
    }
}