<?php

namespace Matcher\Transformer;

use Matcher\TransformerInterface;

class OnlyLetterAndNumberTransformer implements TransformerInterface
{
    public function transform($string)
    {
        return preg_replace('/[^A-Za-zа-яА-Яё0-9]/iu','', $string);
    }
}