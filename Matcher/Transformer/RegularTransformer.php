<?php

namespace Matcher\Transformer;

use Matcher\TransformerInterface;

class RegularTransformer implements TransformerInterface
{
    public function transform($string)
    {
        return '/' . $string . '/';
    }
}