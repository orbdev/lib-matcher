<?php

namespace Matcher;

interface TransformerInterface
{
    public function transform($string);
}