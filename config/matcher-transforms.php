<?php

use Matcher\Transformer\LowerCaseTransformer;
use Matcher\Transformer\OnlyLetterAndNumberTransformer;
use Matcher\Transformer\SimilarLetterTransformer;
use Matcher\Transformer\RegularTransformer;

use Matcher\Entity\Phrase;

return [
    Phrase::TYPE_SIMPLE => [
        [
            'phrase' => [
                LowerCaseTransformer::class,
            ],
            'message' => [
                LowerCaseTransformer::class,
            ],
        ],
    ],
    Phrase::TYPE_REGULAR => [ // Пока нет трансформаций, но должен быть элемент для разового прохождения по массиву
        [
            'phrase' => [

            ],
            'message' => [

            ],
        ],
    ],
    Phrase::TYPE_SMART => [
        [
            'phrase' => [
                OnlyLetterAndNumberTransformer::class,
                SimilarLetterTransformer::class,
                RegularTransformer::class
            ],
            'message' => [
                OnlyLetterAndNumberTransformer::class,
            ],
        ],
    ],
];