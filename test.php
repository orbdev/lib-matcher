<?php

require_once ('bootstrap.php');

use Matcher\Matcher;
use Matcher\Entity\Phrase;

$transformationConfig = require 'config/matcher-transforms.php';

$phrases = [];
$phrases[] = new Phrase(1, 'AliBaba.ru', Phrase::TYPE_SIMPLE);
$phrases[] = new Phrase(2, '/lineage-?(for|4)ever\.ru/i', Phrase::TYPE_REGULAR);
$phrases[] = new Phrase(3, 'zergo.ru', Phrase::TYPE_SMART);
$phrases[] = new Phrase(4, 'z e r g o_r u', Phrase::TYPE_SMART);
$phrases[] = new Phrase(5, 'zergo ru', Phrase::TYPE_SMART);
$phrases[] = new Phrase(6, 'Марко Поло', Phrase::TYPE_SMART);
$phrases[] = new Phrase(7, 'Marko Polo', Phrase::TYPE_SMART);
$phrases[] = new Phrase(8, 'yandex', Phrase::TYPE_SMART);

$phrases[] = new Phrase(9, '694-784-601', Phrase::TYPE_SMART);
$phrases[] = new Phrase(10, '694784601', Phrase::TYPE_SMART);
$phrases[] = new Phrase(11, '694.784.601', Phrase::TYPE_SMART);
$phrases[] = new Phrase(12, '(694) 784-601', Phrase::TYPE_SMART);

$phrases[] = new Phrase(13, 'mmo-auction', Phrase::TYPE_SMART);
$phrases[] = new Phrase(14, 'ммо-аукцион', Phrase::TYPE_SMART);
$phrases[] = new Phrase(15, 'mmo-аукцион', Phrase::TYPE_SMART);

$phrases[] = new Phrase(16, 'ммо-auction', Phrase::TYPE_SMART);

$phrases[] = new Phrase(17, 'pwivi.ru', Phrase::TYPE_SMART);

$phrases[] = new Phrase(18, 'mama', Phrase::TYPE_SMART);

// Если добавить невалидную регулярку - будет исключение:
//$phrases[] = new Phrase(1000, 'some invalid regular expression', Phrase::TYPE_REGULAR);

// Вот это сломает тесты, т.к. I считается синонимом к l
//$phrases[] = new Phrase(2000, 'pwIvI.ru', Phrase::TYPE_SMART);

$matcher = new Matcher($transformationConfig, $phrases);

function out($msg)
{
    echo $msg . "\n";
}

// ТЕСТЫ

// ------------------------------------------------
out('Running test #1');
$result = $matcher->match('some alibAbA.ru news!');
if (count($result) != 1 || reset($result)->getId() != 1) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #2');
$result = $matcher->match('Ждем вас на сервере lineage-4ever.ru');
if (count($result) != 1 || reset($result)->getId() != 2) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #3');
$result = $matcher->match('Ждем вас на сервере lineage-forever.ru');
if (count($result) != 1 || reset($result)->getId() != 2) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #4');
$result = $matcher->match('Ждем вас на сервере Lineageforever.ru');
if (count($result) != 1 || reset($result)->getId() != 2) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #5');
$result = $matcher->match('Ждем вас на сервере lineage-fourever.ru');
if (count($result) != 0) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #6');
$result = $matcher->match('Покупайте напрямую у игроков на бирже ZerGo.ru!');
if (count($result) != 3) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #7');
$result = $matcher->match('Покупайте напрямую у игроков на бирже z e r g o_r u!');
if (count($result) != 3) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #8');
$result = $matcher->match('Покупайте напрямую у игроков на бирже zergo ru!');
if (count($result) != 3) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #9');
$result = $matcher->match('Великий путешественник Mаpko Полo оказал значительное влияние на мореплавателей, картографов, писателей XIV—XVI веков.');
if (count($result) != 1 || reset($result)->getId() != 6) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #10');
$result = $matcher->match('Великий путешественник Маrkо Po10 оказал значительное влияние на мореплавателей, картографов, писателей XIV—XVI веков.');
if (count($result) != 1 || reset($result)->getId() != 7) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #11');
$result = $matcher->match('Супер сервис, поищите в yandex');
if (count($result) != 1 || reset($result)->getId() != 8) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #12');
$result = $matcher->match('Супер сервис, поищите в уаndех');
if (count($result) != 1 || reset($result)->getId() != 8) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #13');
$result = $matcher->match('Супер сервис, поищите в yаndex');
if (count($result) != 1 || reset($result)->getId() != 8) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #14');
$result = $matcher->match('Напишите мне в аську 694-784-601, попробуем договориться');
if (count($result) != 4) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #15');
$result = $matcher->match('Напишите мне в аську 694784601, попробуем договориться');
if (count($result) != 4) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #16');
$result = $matcher->match('Напишите мне в аську 694.784.601, попробуем договориться');
if (count($result) != 4) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #17');
$result = $matcher->match('Напишите мне в аську (694) 784-601, попробуем договориться');
if (count($result) != 4) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #18');
$result = $matcher->match('Попробуйте наш mmo-auction, там вы найдете все самое необходимое!');
if (count($result) != 1 || reset($result)->getId() != 13) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #19');
$result = $matcher->match('Попробуйте наш mmoаукцион, там вы найдете все самое необходимое!');
if (count($result) != 1 || reset($result)->getId() != 15) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #20');
$result = $matcher->match('Попробуйте наш mmo аукцион, там вы найдете все самое необходимое!');
if (count($result) != 1 || reset($result)->getId() != 15) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #21');
$result = $matcher->match('Попробуйте наш mmo-aукциoн, там вы найдете все самое необходимое!');
if (count($result) != 1 || reset($result)->getId() != 15) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #22');
$result = $matcher->match('Попробуйте наш ммо-auction, там вы найдете все самое необходимое!');
if (count($result) != 1 || reset($result)->getId() != 16) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #23');
$result = $matcher->match('Переходите на pwivi.ru!');
if (count($result) == 0) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #24');
$result = $matcher->match('Переходите на pwlvl.ru!');
if (count($result) != 0) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #25');
$result = $matcher->match('Hello, MAMA'); // Латиница, должно поймать
if (count($result) == 0) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #27');
$result = $matcher->match('Hello, MАMА'); // Латиница, русские "А", должно поймать
if (count($result) == 0) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #28');
$result = $matcher->match('Hello, мама'); // Кириллица полностью, не должно поймать
if (count($result) > 0) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}

// ------------------------------------------------
out('Running test #29');
$result = $matcher->match('Hello, мaмa'); // Латинские "a", не должно поймать
if (count($result) > 0) {
    out('Test FAILED');
} else {
    out('Test PASSED');
}